# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2022, CryptPad Team
# This file is distributed under the same license as the CryptPad package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: CryptPad\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-06 10:19+0100\n"
"PO-Revision-Date: 2024-09-12 14:10+0000\n"
"Last-Translator: Suguru Hirahara <shirahara@users.noreply.weblate.cryptpad."
"org>\n"
"Language-Team: Japanese <https://weblate.cryptpad.org/projects/user-guide/"
"instances/ja/>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.6.2\n"
"Generated-By: Babel 2.11.0\n"

#: ../../user_guide/instances.rst:4
msgid "CryptPad instances"
msgstr "CryptPadのインスタンス"

#: ../../user_guide/instances.rst:6
msgid ""
"CryptPad is free software. It can be installed by whoever wants to "
"provide this service in a personal or professional capacity."
msgstr ""
"CryptPadはフリー（自由）ソフトウェアです。個人利用や商業利用でこのサービスを提供したい人は、誰でもこれをインストールすることができます。"

#: ../../user_guide/instances.rst:8
msgid ""
"This documentation is written and maintained by the development team at "
"XWiki SAS in Paris, France. This team administers the flagship instance "
"at `cryptpad.fr <https://cryptpad.fr>`__, but there are many others. This"
" documentation applies to all instances unless explicitly mentioned."
msgstr ""
"このドキュメンテーションは、フランス、パリのXWiki "
"SASの開発チームにより作成、維持されています。チームは `cryptpad.fr "
"<https://cryptpad.fr>`__ のフラッグシップのインスタンスを管理していますが、他"
"にも多くのインスタンスがあります。明確な指示がない限り、このドキュメンテーシ"
"ョンは全てのインスタンスに適用されます。"

#: ../../user_guide/instances.rst:10
msgid ""
"Please note that this documentation is kept up to date with the latest "
"version of CryptPad: |release|. It is possible that other instances are "
"using earlier versions. Each instance can also have specific settings "
"such as storage space, disabled applications, or custom colors/styles."
msgstr ""
"このドキュメンテーションは、CryptPadの最新バージョン |release| に合わせて更新されています。他のインスタンスは、それ以前のバージョンを使"
"用している可能性があります。それぞれのインスタンスは、ストレージの容量、無効とされているアプリケーション、色やスタイルなどに関して異なる設定を採用している"
"場合があります。"

#: ../../user_guide/instances.rst:12
msgid ""
"We maintain a `list of public CryptPad instances "
"<https://cryptpad.org/instances>`_."
msgstr ""
"チームは\\ `CryptPadの公開インスタンスの一覧 <https://cryptpad.org/"
"instances>`_\\ を維持・管理しています。"

#: ../../user_guide/instances.rst:14
msgid ""
"If you are looking to set up a CryptPad instance please refer to the "
":ref:`admin_guide`."
msgstr "CryptPadのインスタンスを設定したい場合は\\ :ref:`管理者ガイド <admin_guide>`"
"\\ をご覧ください。"
